#include "Mic815Tab.h"
#include "Mic815.h"
#include "IsMicroMuted.h"

#include <OpenRGB/SettingsManager.h>

Mic815Tab::Mic815Tab( QWidget *parent ) : QWidget( parent ) {
	setupUi( this );
	LoadDevices();
	LoadLEDs();
}

void Mic815Tab::MicroToggled( bool enabled ) {
	for ( int i = 0; i < kbdList->count(); ++i ) {
		auto kbd = kbdList->item( i );
		if ( kbd->checkState() != Qt::Checked ) continue;

		auto ctrl = getController( i );
		if ( !ctrl ) continue;

		auto color = micColor->color( enabled ? LedColor::ColorType::kEnabled : LedColor::ColorType::kDisabled );
		ApplyColor( ctrl, color );
	}
}

void Mic815Tab::UpdateLeds() {
	for (int i = 0; i < kbdList->count(); ++i){
		auto item = kbdList->item(i);
		if (item->checkState() != Qt::Checked) continue;

		auto ctrl = getController(i);
		auto color = micColor->color( isMicroMuted() ? LedColor::ColorType::kEnabled : LedColor::ColorType::kDisabled );
		ApplyColor( ctrl, color );
	}
}

void Mic815Tab::ApplyColor( RGBController *ctrl, const QColor &color ) {
	for ( int i = -1; auto &&led : ctrl->leds ) {
		++i;

		auto rgbColor = ToRGBColor( color.red(), color.green(), color.blue() );
		bool needUpdate = false;
		for ( auto &&ledId : layout ) {
			if ( ctrl->GetLED( ledId ) != rgbColor ) {
				ctrl->SetLED( ledId, rgbColor );
				needUpdate = true;
			}
		}
		if ( needUpdate ) {
			ctrl->UpdateLEDs();
			kbd->setSelectionColor( rgbColor );
		}
	}
}

void Mic815Tab::Clear() {
	SaveSettings();
	kbdList->clear();
}

void Mic815Tab::LoadDevices() {
	auto ctrls = Mic815::_RM->GetRGBControllers();
	auto sets  = Mic815::_RM->GetSettingsManager();

	for ( auto &&ctrl : ctrls ) {
		if ( ctrl->type != DEVICE_TYPE_KEYBOARD ) continue;
		if ( ctrl->leds.size() <= 1 ) continue;

		kbdList->addItem( ctrl->name.data() );
		auto kbd = kbdList->item( kbdList->count() - 1 );
		kbd->setData( Qt::UserRole, ctrl->serial.data() );

		try {
			auto json = sets->GetSettings( "Mic815" );
			auto set  = json.value<bool>( ctrl->name + "/" + ctrl->serial, false );
			kbd->setCheckState( set ? Qt::Checked : Qt::Unchecked );
		} catch ( json::exception &e ) {
			kbd->setCheckState( Qt::Unchecked );
		}
	}
}

void Mic815Tab::LoadLEDs() {
	auto sets = Mic815::_RM->GetSettingsManager();

	auto readColor = []( const json &json ) -> QColor {
		auto r = json.value<int>( "red", 255 );
		auto g = json.value<int>( "green", 255 );
		auto b = json.value<int>( "blue", 255 );
		return QColor( r, g, b );
	};

	try {
		auto json = sets->GetSettings( "Mic815" );

		micColor->setColor( LedColor::ColorType::kDisabled, readColor( json.value( "disabled", json::object() ) ) );
		micColor->setColor( LedColor::ColorType::kEnabled, readColor( json.value( "enabled", json::object() ) ) );
		auto layout_cxx = json.value<std::vector<int>>( "layout", std::vector<int>() );
		layout			= QVector<int>( layout_cxx.begin(), layout_cxx.end() );
	} catch ( json::exception &e ) {
	}
}

void Mic815Tab::SaveSettings() {
	auto sets = Mic815::_RM->GetSettingsManager();

	auto writeColor = []( const QColor &color ) -> json {
		auto json	  = json::object();
		json["red"]	  = color.red();
		json["green"] = color.green();
		json["blue"]  = color.blue();
		return json;
	};

	try {
		auto json = json::object();

		for ( int i = 0; i < kbdList->count(); ++i ) {
			auto kbd   = kbdList->item( i );
			auto name  = kbd->text().toStdString() + "/" + kbd->data( Qt::UserRole ).toString().toStdString();
			json[name] = kbd->checkState() == Qt::Checked;
		}

		json["disabled"] = writeColor( micColor->color( LedColor::ColorType::kDisabled ) );
		json["enabled"]	= writeColor( micColor->color( LedColor::ColorType::kEnabled ) );
		json["layout"]	= std::vector<int>( layout.begin(), layout.end() );

		sets->SetSettings( "Mic815", json );
	} catch ( json::exception &e ) {
	}
}

void Mic815Tab::changeEvent( QEvent *e ) {
	QWidget::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			retranslateUi( this );
			break;
		default:
			break;
	}
}

void Mic815Tab::hideEvent( QHideEvent * ) {
	SaveSettings();
}

RGBController *Mic815Tab::getController( int row ) {
	if ( row < 0 || row > kbdList->count() ) return nullptr;

	auto &ctrls = Mic815::_RM->GetRGBControllers();

	for ( auto &&ctrl : ctrls ) {
		if ( ctrl->type != DEVICE_TYPE_KEYBOARD ) continue;
		if ( kbdList->item( row )->text() != ctrl->name.data() ) continue;
		if ( kbdList->item( row )->data( Qt::UserRole ).toString() != ctrl->serial.data() ) continue;

		return ctrl;
	}

	return nullptr;
}

void Mic815Tab::on_kbdList_currentRowChanged( int currentRow ) {
	if ( currentRow < 0 || currentRow > kbdList->count() ) return;

	auto ctrl = getController( currentRow );
	if ( !ctrl ) return;

	kbd->setController( ctrl );
	if ( layout.empty() ) {
		for ( int i = 0; i < ctrl->zones.size(); ++i ) {
			kbd->blockSignals( true );
			kbd->selectZone( i, true );
			kbd->blockSignals( false );
		}
	} else {
		kbd->blockSignals( true );
		kbd->selectLeds( layout );
		kbd->blockSignals( false );
	}
}

void Mic815Tab::on_kbd_selectionChanged( QVector<int> select ) {
	auto ctrl = getController( kbdList->currentRow() );
	if ( !ctrl ) return;

	if ( select.empty() ) {
		for ( int i = 0; i < ctrl->zones.size(); ++i ) {
			kbd->blockSignals( true );
			kbd->selectZone( i, true );
			kbd->blockSignals( false );
		}
	} else
		layout = select;
}

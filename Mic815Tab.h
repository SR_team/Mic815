#ifndef MIC815TAB_H
#define MIC815TAB_H

#include "ui_Mic815Tab.h"

class Mic815Tab : public QWidget, private Ui::Mic815Tab {
Q_OBJECT

	QVector<int> layout;

public:
	explicit Mic815Tab( QWidget *parent = nullptr );

public slots:
	void MicroToggled( bool enabled );
	void UpdateLeds();
	void ApplyColor( RGBController *ctrl, const QColor &color );
	void Clear();
	void LoadDevices();
	void LoadLEDs();
	void SaveSettings();

protected:
	void changeEvent( QEvent *e ) override;
	void hideEvent( QHideEvent * ) override;

	class RGBController *getController( int row );
private slots:
	void on_kbdList_currentRowChanged( int currentRow );
	void on_kbd_selectionChanged( QVector<int> );
};

#endif // MIC815TAB_H

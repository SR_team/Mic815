#ifndef LEDCOLOR_H
#define LEDCOLOR_H

#include "ui_LedColor.h"

class LedColor : public QWidget, private Ui::LedColor {
	Q_OBJECT

public:
	explicit LedColor( QWidget *parent = nullptr );

	enum class ColorType : bool { kDisabled = false, kEnabled = true };

	virtual void   setColor( ColorType type, const QColor &color );
	virtual QColor color( ColorType type ) const;

protected:
	void changeEvent( QEvent *e );
};

#endif // LEDCOLOR_H

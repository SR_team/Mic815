#include "Mic815.h"
#include "IsMicroMuted.h"

ResourceManagerInterface *Mic815::_RM = nullptr;

Mic815::Mic815() {
	_allowColorChange = false;

	_thread = std::jthread( [this]( std::stop_token stoken ) { loop( stoken ); } );
}

Mic815::~Mic815() {
	_thread.request_stop();
	_thread.join();
}

OpenRGBPluginInfo Mic815::GetPluginInfo() {
	PInfo.Name = "Mic815";
	PInfo.Description = "Change color for micro key.";
	PInfo.Location = OPENRGB_PLUGIN_LOCATION_TOP;
	PInfo.Label = "Mic815";

	return PInfo;
}

unsigned int Mic815::GetPluginAPIVersion() {
	return OPENRGB_PLUGIN_API_VERSION;
}

void Mic815::Load( ResourceManagerInterface *resource_manager_ptr ) {
	_RM = resource_manager_ptr;
	_RM->WaitForDeviceDetection();

	_RM->RegisterDetectionStartCallback( &Mic815::DetectionStart, this );
	_RM->RegisterDetectionEndCallback( &Mic815::DetectionEnd, this );
}

QWidget *Mic815::GetWidget() {
	_allowColorChange = true;

	_ui = new Mic815Tab();

	QObject::connect( this, &Mic815::onMicroToggled, _ui, &Mic815Tab::MicroToggled );
	QObject::connect( this, &Mic815::onUpdate, _ui, &Mic815Tab::UpdateLeds );
	QObject::connect( this, &Mic815::onDevClear, _ui, &Mic815Tab::Clear );
	QObject::connect( this, &Mic815::onDevReady, _ui, &Mic815Tab::LoadDevices );

	_prevMicro = !isMicroMuted();

	return _ui;
}

QMenu *Mic815::GetTrayMenu() {
	return nullptr;
}

void Mic815::Unload() {
	_ui = nullptr;
}

void Mic815::loop( std::stop_token stoken ) {
	while ( !stoken.stop_requested() ) {
		std::this_thread::sleep_for( kTimeout );
		if ( !_allowColorChange ) continue;

		bool anyChanged = false;
		bool micro = isMicroMuted();

		if ( micro != _prevMicro ) {
			anyChanged = true;
			_prevMicro = micro;
			emit onMicroToggled( micro );
		}
		if ( !anyChanged ) emit onUpdate();
	}
}

void Mic815::devClear() {
	emit onDevClear();
}

void Mic815::devReady() {
	emit onDevReady();
}

void Mic815::DetectionStart( void *_this ) {
	auto self = (Mic815 *)_this;

	self->_allowColorChange = false;
	QMetaObject::invokeMethod( self, "devClear", Qt::QueuedConnection );
}

void Mic815::DetectionEnd( void *_this ) {
	auto self = (Mic815 *)_this;

	self->_allowColorChange = true;
	QMetaObject::invokeMethod( self, "devReady", Qt::QueuedConnection );
}

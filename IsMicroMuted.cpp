#include "IsMicroMuted.h"
#include "pamixer/pulseaudio.hh"

bool isMicroMuted() {
	static Pulseaudio pulse( "Mic815" );
	if ( pulse.state != CONNECTED ) {
		pulse.~Pulseaudio();
		pulse = Pulseaudio( "Mic815" );
	}

	return pulse.state == CONNECTED && pulse.get_default_source().mute;
}

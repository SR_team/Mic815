#ifndef MIC815_GLOBAL_H
#define MIC815_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(MIC815_LIBRARY)
#  define MIC815_EXPORT Q_DECL_EXPORT
#else
#  define MIC815_EXPORT Q_DECL_IMPORT
#endif

#endif // MIC815_GLOBAL_H

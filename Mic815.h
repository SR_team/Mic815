#ifndef MIC815_H
#define MIC815_H

#include "Mic815_global.h"

#include <atomic>
#include <chrono>
#include <thread>

#include <QObject>

#include <OpenRGBPluginInterface.h>

#include "Mic815Tab.h"

class MIC815_EXPORT Mic815 : public QObject, public OpenRGBPluginInterface {
	Q_OBJECT
	Q_PLUGIN_METADATA( IID OpenRGBPluginInterface_IID )
	Q_INTERFACES( OpenRGBPluginInterface )

	constexpr static std::chrono::milliseconds kTimeout{ 50 };

public:
	Mic815();
	~Mic815();

	OpenRGBPluginInfo PInfo;
	OpenRGBPluginInfo GetPluginInfo() override;
	unsigned int GetPluginAPIVersion() override;
	void Load( ResourceManagerInterface *resource_manager_ptr ) override;
	QWidget *GetWidget() override;
	QMenu *GetTrayMenu() override;
	void Unload() override;

	static ResourceManagerInterface *_RM;
	Mic815Tab *_ui = nullptr;

protected:
	std::atomic<bool> _allowColorChange;
	std::jthread _thread;

	bool _prevMicro = false;

	void loop( std::stop_token stoken );

public slots:
	void devClear();
	void devReady();

signals:
	void onMicroToggled( bool enabled );
	void onUpdate();
	void onDevClear();
	void onDevReady();

private:
	static void DetectionStart( void *_this );
	static void DetectionEnd( void *_this );
};

#endif // MIC815_H
